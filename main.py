__author__ = "Frank Schoeneman"
__copyright__ = "Copyright (c) 2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Frank Schoeneman"
__email__ = "fvschoen@buffalo.edu"
__status__ = "Development"


import argparse
import time
import os
import logging

import my_util
import init_matrix
import collect_bc
import eigensolvers

from pyspark import SparkContext, SparkConf
from pyspark import StorageLevel as stglev


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--num_points", help="Number of points.", type=int, required=True)
    parser.add_argument("-b", "--block_size", help="Submatrix block size.", type=int, required=True)
    parser.add_argument("-p", "--partitions", help="Number of partitions.", type=int, required=True)
    parser.add_argument("-F", "--partitioner", help="Partitioning Function. [md or ph]", type=str, required=True)

    parser.add_argument("-f", "--input_file", help="Input data. (.tsv format)", type=str, required=True)


    parser.add_argument("-D", "--highdim", help="Input data dimensionality.", type=int, required=True)
    parser.add_argument("-k", "--ngbr_size", help="Neighborhood size.", type=int, required=True)
    parser.add_argument("-d", "--lowdim", help="Reduced dimensionality.", type=int, required=True)

    parser.add_argument("-l", "--powiters", help="Maximum iterations for power iteration.", type=int, required=True)
    parser.add_argument("-t", "--conv", help="Convergence threshold for power iteration.", type=int, required=True)

    parser.add_argument("-o", "--output_file", help="Output file name. ", type=str, required=False)
    parser.add_argument("-e", "--log_dir", help="Spark event log dir.", type=str, required=False)

    args = parser.parse_args()
    n = args.num_points
    b = args.block_size
    p = args.partitions
    D = args.highdim
    k = args.ngbr_size
    d = args.lowdim
    l = args.powiters
    t = args.conv
    F = args.partitioner.lower()
    input_file = args.input_file

    q, N = my_util.block_vars(n, b)
    t = 10**(-1.*t)
    rdd_partitioner = my_util.verify_partitioner(F, q)

    conf = SparkConf()

    # optional log for history server
    save_history = args.log_dir is not None
    if save_history:
        conf.set("spark.eventLog.enabled", "true")\
            .set("spark.eventLog.dir", args.log_dir)\
            .set("spark.history.fs.logDirectory", args.log_dir)

    write_file = args.output_file is not None
    output_file = None
    if write_file: output_file = args.output_file

    sc = SparkContext(conf=conf)
    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("IsomapSpark")
    logger.setLevel(sc._jvm.org.apache.log4j.Level.ALL)
    logger.info('n: {}, b: {}, q: {}, p: {}, partitioner: {}'.format(n, b, q, p, F))
    logger.info('D: {}, d: {}, k: {}, l: {}, t: {}'.format(D, d, k, l, t))

    ti = time.time()
    t0 = time.time()
    # load data, construct pwd matrix, compute knn, return adj mtx
    data_blocks = init_matrix.load_data(b, input_file, D, sc)
    pwd_blocks  = init_matrix.compute_block_pwd(data_blocks, q, p, rdd_partitioner)
    local_min_knn = init_matrix.min_local_knn(pwd_blocks, k)
    row_red_knn = init_matrix.row_red_all_knn(local_min_knn, k, rdd_partitioner, p)
    block_red_knn = init_matrix.re_assign_key(row_red_knn)
    global_knn = init_matrix.reduce_global_knn(pwd_blocks, block_red_knn, p, rdd_partitioner)
    global_knn.persist(stglev.MEMORY_AND_DISK)
    global_knn.count()
    t1 = time.time()
    logger.info("Time for kNN search: " + str(t1-t0) + " s.")    

    
    t0 = time.time()
    # run apsp-solver Blocked-CB
    blocks_dir = 'testdir/'
    os.system("mkdir "+blocks_dir)
    apsp_graph = collect_bc.collect_bc_block_fw(global_knn, q, p, rdd_partitioner, blocks_dir, sc)
    apsp_graph.persist(stglev.MEMORY_AND_DISK)
    apsp_graph.count()
    t1 = time.time()
    os.system("rm -r "+blocks_dir)
    logger.info("Time for Blocked-CB solution: " + str(t1-t0) + " s.")

    t0 = time.time()
    feat_matrix = init_matrix.square_matrix_values(apsp_graph)
    dbl_ctr_mtx = init_matrix.double_center(n, feat_matrix, p, sc)
    t1 = time.time()
    logger.info("Time for matrix normalization: " + str(t1-t0) + " s.")

    t0 = time.time()
    lowd_coords = eigensolvers.sim_power_iter(dbl_ctr_mtx, n, b, d, t, l, output_file, sc, logger)
    t1 = time.time()
    logger.info("Time for eigensolver: " + str(t1-t0) + " s.")

    tf = time.time()
    sc.stop()
    logger.info("Time for complete Isomap method: " + str(t1-t0) + "s.")

    logger.info('done!')
    
            
        
