__author__ = "Frank Schoeneman"
__copyright__ = "Copyright (c) 2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Frank Schoeneman"
__email__ = "fvschoen@buffalo.edu"
__status__ = "Development"

import numpy as np
from scipy.spatial.distance import cdist as sp_cdist#, pdist as sp_pdist, squareform

import heapq
from operator import add as _ADD_
from itertools import izip

from pyspark import StorageLevel as stglev


# load data from tsv
def load_data(b, input_file, D, sc):

    def tsv_block((pttnId, linelist), D):
        stringmat = np.fromstring("\t".join([st for st in linelist]), dtype=np.double, sep='\t')
        nx = stringmat.shape[0] / D
        stringmat = stringmat.reshape((nx, D))
        return (pttnId, stringmat)

    # load data into blocks from textfile
    data_blocks = sc.textFile(input_file)\
                   .zipWithIndex()\
                   .map(lambda x : (x[1] / b, x[0]))\
                   .combineByKey(\
                    (lambda x : [x]),\
                    (lambda x, y : x + [y]),\
                    (lambda x, y : x + y))\
                   .map(lambda x : tsv_block(x, D))#\
    return data_blocks

# subblocks of pwd matrix
def compute_block_pwd(data_blocks, q, p, rdd_partitioner):

    def send_blk_to_blk((blkId, dataSub)):
        [(yield (tuple(sorted((blkId, i))), (dataSub, blkId))) for i in range(0, q, 1)]

    def do_block_pwd(iter_):
        for ((I_, J_), x) in iter_:
            if len(x) == 1:
                yield((I_, J_), sp_cdist(x[0][0], x[0][0], metric='euclidean'))
            elif x[0][1] < x[1][1]:
                yield((I_, J_), sp_cdist(x[0][0], x[1][0], metric='euclidean'))
            else:
                yield((I_, J_), sp_cdist(x[1][0], x[0][0], metric='euclidean'))

    pwd_blocks = data_blocks.flatMap(lambda x : send_blk_to_blk(x))\
                 .combineByKey((lambda x : [x]),\
                              (lambda x, y : x + [y]),\
                              (lambda x, y : x + y),
                               numPartitions=p,
                               partitionFunc=rdd_partitioner)\
                 .mapPartitions(lambda x : do_block_pwd(x),\
                               preservesPartitioning=False)
    return pwd_blocks

# local knn to block
def min_local_knn(pwd_blocks, k):
    def comp_sub_knn(((i_, j_), localPwdMat), k):
        selfDist = False
        if i_ == j_: 
            selfDist = True
        if k < localPwdMat.shape[1]:
            argpk = np.argpartition(localPwdMat, k, axis=1)
            [(yield ((i_, local_i), (j_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat[np.arange(localPwdMat.shape[0])[:, None], argpk[:, :k]], argpk[:, :k]))]
        else:
            argpk = np.argpartition(localPwdMat, localPwdMat.shape[1]-1, axis=1)
            [(yield ((i_, local_i), (j_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat[np.arange(localPwdMat.shape[0])[:, None], argpk], argpk))]

        # also yield for (j, i) [upper triangular]
        if (k < localPwdMat.shape[0]) and not selfDist:
            argpkT = np.argpartition(localPwdMat.T, k, axis=1)
            [(yield ((j_, local_i), (i_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat.T[np.arange(localPwdMat.shape[1])[:, None], argpkT[:, :k]], argpkT[:, :k]))]

        elif not selfDist:
            argpkT = np.argpartition(localPwdMat.T, localPwdMat.shape[0]-1, axis=1)
            [(yield ((j_, local_i), (i_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat.T[np.arange(localPwdMat.shape[1])[:, None], argpkT], argpkT))]
        # (block_i, local_i), (block_j, (dists, [args -- local_j?]))

    local_min_knn = pwd_blocks.flatMap(lambda x : comp_sub_knn(x, k),\
                               preservesPartitioning=False)
    return local_min_knn 


# compute global knn
def row_red_all_knn(local_min_k, k, rdd_partitioner, p):
    def zip_sub_row((block_j, (dvals, didx))):
        return zip( dvals, zip(block_j * np.ones(len(didx), dtype=int), didx))
        # returns [(distval, (block_j, local_j))]

    def merge_and_take_k_small_M(x, y, k):
        if isinstance(y, tuple):
            (block_j, (dvals, didx)) = y
            y = zip( dvals, zip(block_j * np.ones(len(didx), dtype=int), didx))
        mergedL = x + y
        heapq.heapify(mergedL)            
        return heapq.nsmallest(k, mergedL)        

    def merge_and_take_k_small(x, y, k):
        mergedL = x + y
        heapq.heapify(mergedL)
        return heapq.nsmallest(k, mergedL)

    row_red_knn = local_min_k.combineByKey(\
                (lambda x : zip_sub_row(x)),\
                (lambda x, y : merge_and_take_k_small_M(x, y, k)),\
                (lambda x, y : merge_and_take_k_small(x, y, k)),\
                numPartitions=p,\
                partitionFunc=rdd_partitioner)
    return row_red_knn

# change key to send knn val back to block
def re_assign_key(row_red_knn):

    def changeKey(((block_i, local_i), list_of_kNN_from_row)): # (dist, (block_j, didx))
        for (distval, (block_j, local_j)) in list_of_kNN_from_row:
            if block_i <= block_j: 
                block_key = (block_i, block_j)
                mat_entry = (local_i, local_j, distval)
                yield (block_key, mat_entry)
                if block_i == block_j:
                    mat_entry2 = (local_j, local_i, distval)
                    yield (block_key, mat_entry2)
            else:
                block_key = (block_j, block_i)
                mat_entry = (local_j, local_i, distval)
                yield (block_key, mat_entry)

    #changekey to ((block_i, block_j), (local_i, local_j, distval))
    block_red_knn = row_red_knn.flatMap(lambda x : changeKey(x),\
                    preservesPartitioning=False)
    return block_red_knn

# final knn step
def reduce_global_knn(pwd_blocks, block_red_knn, p, rdd_partitioner):

    def mat_comb(x):
        if isinstance(x, np.ndarray):
            x[:, :] = np.inf
            return x
        else: return [x]

    def update_mat_list(M_, L_):
        r_i, c_j, d_v = zip(*L_)  
        M_[r_i, c_j] = d_v
        return M_                    

    def merge_mat_kNN(x, y):
        if isinstance(x, np.ndarray):
            return update_mat_list(x, [y])
        if isinstance(y, np.ndarray):
            return update_mat_list(y, x)
        return [y] + x

    def merge_mat_K_comb(x, y):
        if isinstance(x, np.ndarray):
            return update_mat_list(x, y)
        if isinstance(y, np.ndarray):
            return update_mat_list(y, x)
        return y + x

    global_knn = pwd_blocks.union(block_red_knn)\
       .combineByKey(\
        (lambda x : mat_comb(x)),\
        (lambda x, y : merge_mat_kNN(x, y)),\
        (lambda x, y : merge_mat_K_comb(x, y)),\
        numPartitions=p,\
        partitionFunc=rdd_partitioner)
    return global_knn

# square feature matrix
def square_matrix_values(apsp_graph):

    def do_square_block(iter_):
        for ((I_, J_), blockMat) in iter_:
           yield ((I_, J_), np.square(blockMat, out=blockMat))

    apsp_graph = apsp_graph.mapPartitions(lambda x : do_square_block(x),\
                      preservesPartitioning=False)
    return apsp_graph


def double_center(n, feat_matrix, p, sc):

    def comp_norm_sums(((I_, J_), localMat)):
        if I_ == J_:
            yield (J_, np.sum(localMat, axis=0))
        else:
            yield (J_, np.sum(localMat, axis=0))
            yield (I_, np.sum(localMat, axis=1))

    def grand_sum(iter_):
        partTotalSum = 0.
        for (J_, subColSum) in iter_:
            partTotalSum += np.sum(subColSum)
        yield partTotalSum
        
    col_sums = feat_matrix.flatMap(lambda x : comp_norm_sums(x),\
                                preservesPartitioning=False )\
                       .combineByKey((lambda x : x),\
                                     (lambda x, y: x + y),\
                                     (lambda x, y: x + y),\
                                      numPartitions=p)\
                       .persist(stglev.MEMORY_AND_DISK) # used twice below

    grand_mean = col_sums.mapPartitions(lambda x : grand_sum(x)).reduce(_ADD_) / (n*n)

    cRM = col_sums\
                 .mapPartitions(lambda x : [(yield ((x_[0], x_[1] / (1. * n) ))) for x_ in x])\
                 .collectAsMap()

    means_bc = sc.broadcast((grand_mean, cRM))
    

    def dbl_center(iter_):
        gMean, colRowMeans = means_bc.value        
        for ((I_, J_), locSubMat) in iter_:
            locSubMat -= colRowMeans[J_]
            locSubMat -= colRowMeans[I_].reshape((locSubMat.shape[0], 1))
            locSubMat += gMean
            yield ((I_, J_), -.5*locSubMat) # * -.5


    dbl_cent_matrix = feat_matrix.mapPartitions(lambda x : dbl_center(x))\
                               .persist(stglev.MEMORY_AND_DISK)
    col_sums.unpersist()
    means_bc.unpersist()
    return dbl_cent_matrix    











