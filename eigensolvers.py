import numpy as np

from collections import OrderedDict
from operator import add as _ADD_

def sim_power_iter(dbl_ctr_mtx, n, b, d, t, l, output_file, sc, logger):

    def blockify(D):
        return [((i, 0), D[a:(a+b), :]) for i, a in enumerate(range(0, n, b))]

    def matrix_multiply(Amat, rightMat):
        piVec = rightMat.value

        def multBlocks(((I_, J_), subMatrix)):
            # map block (i, j) as (i, j) and (j, i) 
            # multiplied to appropriate block
            # of rightMat (bc)
            yield ((I_, 0), np.dot(subMatrix, piVec[(J_, 0)]))
            if I_ != J_: yield ((J_, 0), np.dot(subMatrix.T, piVec[(I_, 0)]) ) # check performance

        # combineByKey to reduce (+)
        # blocks (i, 0) of new V vector
        # return this resulting RDD
        return Amat.flatMap(lambda x : multBlocks(x)).reduceByKey(_ADD_) # RDD of results

    # initial guess 
    Q_, _ = np.linalg.qr(np.eye(n, d), mode='reduced')

    Q_dict = OrderedDict(sorted(blockify(Q_), key=lambda t : t[0]) )
    Q_last = np.vstack(Q_dict.values())

    for i in range(l):    

        V_bc = sc.broadcast(Q_dict)

        V_ = matrix_multiply(dbl_ctr_mtx, V_bc)
        V_bc.unpersist()

        V_ = OrderedDict(sorted(V_.collectAsMap().items(), key=lambda t : t[0]) )
        V_ = np.vstack((V_.values()))
        Q_, R_ = np.linalg.qr(V_, mode='reduced')

        err_ = np.sum(np.square(Q_ - Q_last))
        if i % 5 == 0:
            logger.info("Simultaneous Power Iteration error at iteration " \
                  + str(i) + " is = " +str(err_))
        
        Q_dict = OrderedDict(sorted(blockify(Q_), key=lambda t : t[0]) )
        Q_last = np.vstack(Q_dict.values())

        if err_ < t: break # converged

    # after convergence
    # Columns of Q_ are eigenvectors, Diagonal of R_ contains eigenvalues
    # dCoords = Q_ .* ( ones(n, 1) * sqrt(np.diag(R_)) )
    # dCoords = np.multiply( Q_, np.outer(np.ones((n, 1)), np.diag(R_)) )

    dCoords = Q_ * np.sqrt(np.diag(R_))
    if output_file is not None: np.savetxt(output_file, dCoords, fmt='%6f', delimiter='\t')

    return dCoords

