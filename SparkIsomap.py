import numpy as np
from scipy.spatial.distance import cdist as sp_cdist, pdist as sp_pdist, squareform
from numpy import linalg as LA
from scipy.linalg import blas as FB
from scipy.sparse.csgraph import floyd_warshall as apsp_fw

from pyspark import SparkContext, SparkConf#, TaskContext
from pyspark.files import SparkFiles as sparkf
from pyspark import TaskContext
from pyspark import StorageLevel as stglev

import numba as nb
from itertools import izip
import argparse
import time
import heapq
import math
from operator import add as _ADD_
from collections import OrderedDict

# for debugging
np.random.seed(seed=1)

def subBlk_to_PtBlk((k1, k2)):
    return (k1 / b_hat, k2 / b_hat)

def uppertri_idx((k1, k2)):
    return (q_hat * k1) + k2 - ((k1 * (k1 + 1)) / 2)

def div_blocksPP(k1):
    return k1 / blocksPP

def blockBlockPartition(_KEY_):
    return div_blocksPP(uppertri_idx(subBlk_to_PtBlk(_KEY_)))

'''
Set custom partitonFunc to be used.
'''
def custom_partitioner((k1, k2)):
#    return uppertri_idx(fractalPartition((k1 + 1, k2 + 1)))
    return blockBlockPartition((k1, k2))

'''
 Min-plus matrix multiplication of matrices 
 A and B. Compiled for better performance using numba.
'''
@nb.jit(nopython=True)
def mpmatmul(A, B):
    assert A.shape[1] == B.shape[0], 'Matrix dimension mismatch in mpmatmul().'

    C = np.zeros((A.shape[0], B.shape[1]))
    for i in range(A.shape[0]):
        for j in range(B.shape[1]):
            somesum = np.inf
            for k in range(A.shape[1]):
                somesum = min(somesum, A[i, k] + B[k, j])
            C[i, j] = somesum
    return C

'''
 Min-plus matrix multiplication of matrices 
 A*B = C. D_ returned is elementwise min of C and input D_.
 Compiled for better performance using numba.
 Input matrices A, B stored C, Fortran contiguous, respectively, for best performance. 
'''
def minmpmatmul(A, B, D_):
    return _minmpmatmul(np.ascontiguousarray(A), np.asfortranarray(B), D_)
@nb.jit(nopython=True)
def _minmpmatmul(A, B, D_):
    assert A.shape[1] == B.shape[0], 'Matrix dimension mismatch in minmpmatmul().'

    for i in range(A.shape[0]):
        for j in range(B.shape[1]):
            somesum = np.inf
            for k in range(A.shape[1]):
                somesum = min(somesum, A[i, k] + B[k, j])
            D_[i, j] = min(D_[i, j], somesum)
    return D_


if __name__ == "__main__":

    t_init = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", help="Input data (.tsv format).")
    parser.add_argument("-o", help="Output file name.")
    parser.add_argument("-e", help="Spark event log directory.")
    parser.add_argument("-C", help="Spark checkpoint directory.")

    parser.add_argument("-b", help="Submatrix block size.")
    parser.add_argument("-p", help="Number of partitions.")

    parser.add_argument("-n", help="Number of points.")
    parser.add_argument("-D", help="Input data dimensionality.")
    parser.add_argument("-k", help="Neighborhood size.")
    parser.add_argument("-d", help="Reduced dimensionality.")

    parser.add_argument("-l", help="Maximum iterations for power iteration.")
    parser.add_argument("-t", help="Convergence threshold for power iteration.")

    args = parser.parse_args()
    if args.e:
       e = args.e#'file:///' + args.e
       print 'Save history-server file to:', e
    if args.f:
       data_input = args.f
    if args.o:
        output_dir = args.o
    if args.k:
        k = int(args.k)
    if args.n:
        n = int(args.n)
    if args.d:
        d = int(args.d)
    if args.D:
        D = int(args.D)
    if args.b:
        b = int(args.b)
    if args.B:
        B = int(args.B)
    if args.p:
        p = int(args.p)
    if args.t:
        t = int(args.t)
    if args.l:
        l = int(args.l)

    if args.C:
        CHKPT_DIR = args.C

    # set param for power iteration
    THRESH_POWER_ITER = 10**(-1.*t)
    MAX_POWER_ITER = l

    # set block dims, partitions, etc
    q = int((n-1) / b) + 1

    b_hat = 1
    q_hat = int(np.ceil(1.*q / b_hat))
    
    N = (q * (q+1)) / 2
    if N >= p:
        blocksPP = N / p
    else: 
        blocksPP = 1

    print 'Spark-Isomap configuration and parameters:\n'
    print 'n:', n, 'D:', D, 'd:', d, 'k:', k
    print

    print 'Simultaneous Power Iteration:\n'
    print 'Max iterations:', MAX_POWER_ITER
    print 'Convergence threshold:', THRESH_POWER_ITER
    print

    print 'Matrix distribution and partitioning:\n'
    print 'b:', b, '|', 'q:', q
    print 'b_hat:', b_hat, '|',  'q_hat:', q_hat
    print 'Number of submatrices:', N
    print 'Number of partitions:', p
    print 'Blocks per partititon:', blocksPP
    print

    conf = SparkConf().set("spark.driver.maxResultSize", "40g")\
    .set("spark.eventLog.enabled", "true")\
    .set("spark.eventLog.dir", e)\
    .set("spark.history.fs.logDirectory", e)\

    sc = SparkContext(conf=conf)
    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("Large Scale Isomap")

    sc.setCheckpointDir(CHKPT_DIR)

    print 'Startup time:', time.time() - t_init 
    print 'SPARK VERSION:', sc.version
    print

    t_start_isomap = time.time()

    def loadDataBlocks(D, b):
        def tsvBlock((pttnId, linelist), D):
            stringmat = np.fromstring("\t".join([st for st in linelist]), dtype=np.double, sep='\t')
            nx = stringmat.shape[0] / D
            stringmat = stringmat.reshape((nx, D))
            return (pttnId, stringmat)

        # load data into blocks from textfile
        dataBlks = sc.textFile(data_input)\
                       .zipWithIndex()\
                       .map(lambda x : (x[1] / b, x[0]))\
                       .combineByKey(\
                        (lambda x : [x]),\
                        (lambda x, y : x + [y]),\
                        (lambda x, y : x + y))\
                       .map(lambda x : tsvBlock(x, D))#\
        return dataBlks
    # NO GUARANTEE ON ORDER OF DATA LINES AFTER CombineByKey
    # Add sorted function to preserve order if important.

    dataBlocks = loadDataBlocks(D, b)

    def computeBlockPWD(dblks):

        def sendBlkToBlk((blkId, dataSub)):
            [(yield (tuple(sorted((blkId, i))), (dataSub, blkId))) for i in range(0, q, 1)]
    
        def doBlockPWD(iter_):
            for ((I_, J_), x) in iter_:
                if len(x) == 1:
                    yield((I_, J_), sp_cdist(x[0][0], x[0][0], metric='euclidean'))
                elif x[0][1] < x[1][1]:
                    yield((I_, J_), sp_cdist(x[0][0], x[1][0], metric='euclidean'))
                else:
                    yield((I_, J_), sp_cdist(x[1][0], x[0][0], metric='euclidean'))

        pwdb = dblks.flatMap(lambda x : sendBlkToBlk(x))\
                    .combineByKey((lambda x : [x]),\
                                  (lambda x, y : x + [y]),\
                                  (lambda x, y : x + y),
                                   numPartitions=p,
                                   partitionFunc=custom_partitioner)\
                    .mapPartitions(lambda x : doBlockPWD(x),\
                                   preservesPartitioning=False)\
                    .persist(stglev.MEMORY_AND_DISK)
        return pwdb

    pwdBlock = computeBlockPWD(dataBlocks)
    

    def minimizeLocalkNN(pwdb, k):
        def compSubKnn(((i_, j_), localPwdMat), k):
            selfDist = False
            if i_ == j_: 
                selfDist = True
            if k < localPwdMat.shape[1]:
                argpk = np.argpartition(localPwdMat, k, axis=1)
                [(yield ((i_, local_i), (j_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat[np.arange(localPwdMat.shape[0])[:, None], argpk[:, :k]], argpk[:, :k]))]
            else:
                argpk = np.argpartition(localPwdMat, localPwdMat.shape[1]-1, axis=1)
                [(yield ((i_, local_i), (j_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat[np.arange(localPwdMat.shape[0])[:, None], argpk], argpk))]

            # also yield for (j, i) [upper triangular]
            if (k < localPwdMat.shape[0]) and not selfDist:
                argpkT = np.argpartition(localPwdMat.T, k, axis=1)
                [(yield ((j_, local_i), (i_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat.T[np.arange(localPwdMat.shape[1])[:, None], argpkT[:, :k]], argpkT[:, :k]))]

            elif not selfDist:
                argpkT = np.argpartition(localPwdMat.T, localPwdMat.shape[0]-1, axis=1)
                [(yield ((j_, local_i), (i_, rowentry))) for (local_i, rowentry) in enumerate(izip(localPwdMat.T[np.arange(localPwdMat.shape[1])[:, None], argpkT], argpkT))]
            # (block_i, local_i), (block_j, (dists, [args -- local_j?]))

        lmk = pwdb.flatMap(lambda x : compSubKnn(x, k),\
                         preservesPartitioning=False)
        return lmk 

    localMinK = minimizeLocalkNN(pwdBlock, k)

    def rowReduceAllkNN(lmk, k):
        def zipSubRow((block_j, (dvals, didx))):
            return zip( dvals, zip(block_j * np.ones(len(didx), dtype=int), didx))
            # returns [(distval, (block_j, local_j))]

        def mergeAndTakeKsmallM(x, y, k):
            if isinstance(y, tuple):
                (block_j, (dvals, didx)) = y
                y = zip( dvals, zip(block_j * np.ones(len(didx), dtype=int), didx))
            mergedL = x + y
            heapq.heapify(mergedL)            
            return heapq.nsmallest(k, mergedL)        

        def mergeAndTakeKsmall(x, y, k):
            mergedL = x + y
            heapq.heapify(mergedL)
            return heapq.nsmallest(k, mergedL)

        rrk = lmk.combineByKey(\
                    (lambda x : zipSubRow(x)),\
                    (lambda x, y : mergeAndTakeKsmallM(x, y, k)),\
                    (lambda x, y : mergeAndTakeKsmall(x, y, k)),\
                    numPartitions=p,\
                    partitionFunc=custom_partitioner)
        return rrk

    rowReducedKnn = rowReduceAllkNN(localMinK, k)

    def reAssignKey(rrk):

        def changeKey(((block_i, local_i), list_of_kNN_from_row)): # (dist, (block_j, didx))
            for (distval, (block_j, local_j)) in list_of_kNN_from_row:
                if block_i <= block_j: 
                    block_key = (block_i, block_j)
                    mat_entry = (local_i, local_j, distval)
                    yield (block_key, mat_entry)
                    if block_i == block_j:
                        mat_entry2 = (local_j, local_i, distval)
                        yield (block_key, mat_entry2)
                else:
                    block_key = (block_j, block_i)
                    mat_entry = (local_j, local_i, distval)
                    yield (block_key, mat_entry)
    
        #changekey to ((block_i, block_j), (local_i, local_j, distval))
        brk = rrk.flatMap(lambda x : changeKey(x),\
                        preservesPartitioning=False)
        return brk

    blockReducedKnn = reAssignKey(rowReducedKnn)

    def reduceGlobalkNN(pwdblk, brk):

        def matComb(x):
            if isinstance(x, np.ndarray):
                x[:, :] = np.inf
                return x
            else: return [x]

        def updateMatList(M_, L_):
            r_i, c_j, d_v = zip(*L_)  
            M_[r_i, c_j] = d_v
            return M_                    

        def mergeMatkNN(x, y):
            if isinstance(x, np.ndarray):
                return updateMatList(x, [y])
            if isinstance(y, np.ndarray):
                return updateMatList(y, x)
            return [y] + x

        def mergeMatKComb(x, y):
            if isinstance(x, np.ndarray):
                return updateMatList(x, y)
            if isinstance(y, np.ndarray):
                return updateMatList(y, x)
            return y + x

        gk = pwdblk.union(brk)\
           .combineByKey(\
            (lambda x : matComb(x)),\
            (lambda x, y : mergeMatkNN(x, y)),\
            (lambda x, y : mergeMatKComb(x, y)),\
            numPartitions=p,\
            partitionFunc=custom_partitioner)\
           .persist(stglev.MEMORY_AND_DISK)
        return gk

    kNNGraph = reduceGlobalkNN(pwdBlock, blockReducedKnn)

    kNNGraph = kNNGraph.persist(stglev.MEMORY_AND_DISK)
    kNNGraphCount = kNNGraph.count()     
    print 'kNN count', kNNGraphCount

    print
    print 'Brute-Force kNN Runtime:', time.time() - t_init, 's.'

    print 'Brute-force kNN completed.'
    print
    print '- - - - - - - - - - - - - - - - - - - - - - - - - - - -'
    print    

    print
    print 'Beginning APSP stage.'
    print
    print 'requires q =', q, 'iterations.'
    print
    t_init_apsp = time.time()
    print

    apspGraph = kNNGraph

    def scipy_floyd(ADJ_MAT):
        return apsp_fw(ADJ_MAT, directed=False, unweighted=False, overwrite=True)

    def run_APSP_diag_iteration(q_, apspGraph):

        def share_diag_block((blockId, ADJ_MAT), q_):
            diagUpdate = scipy_floyd(ADJ_MAT)
            q_, q_ = blockId
            yield ((q_, q_), (-2, diagUpdate, 0))
            [(yield ((q_, j), (-1, diagUpdate, 1))) for j in range(q_ + 1, q, 1)]
            [(yield ((i, q_), (-1, diagUpdate, -1))) for i in range(0, q_, 1)]

        def updateAndshare(((I_, J_), xx), q_, q):
            if isinstance(xx[0], np.ndarray):
                (InitBlock, (typeFlag, diagPass, matAlign)) = xx
            else:
                ((typeFlag, diagPass, matAlign), InitBlock) = xx

            if typeFlag == -2: submatrix = diagPass
            elif matAlign == 1: submatrix = minmpmatmul(diagPass, InitBlock, InitBlock)
            else:# matAlign == -1:
                submatrix = minmpmatmul(InitBlock, diagPass, InitBlock)

            yield ((I_, J_), submatrix) # save self - diag and row/col
            if I_ == q_ and J_ != q_:
                # RHS Updates
                [(yield ((i, J_), (submatrix, 1))) for i in range(J_+1) if i != q_]
                # LHS Updates
                [(yield ((J_, j), (submatrix.T, -1))) if j!= J_ \
                  else (yield ((J_, j), (submatrix.T, 2)))\
                  for j in range(J_, q, 1)]

            elif J_ == q_ and I_ != q_:
                # RHS Updates
                [(yield ((i, I_), (submatrix.T, 1))) for i in range(0, I_+1)]
                # LHS Updates
                [(yield ((I_, j), (submatrix, -1))) if j!= I_ \
                  else (yield ((I_, j), (submatrix, -2)))\
                  for j in [xx for xx in range(I_, q) if xx != J_]]

        def UpdateMat(iter_, q_):
            for ((I_, J_), x) in iter_:
                if I_ == q_ or J_ == q_: # pass phase 1 and 2 blocks
                    yield ((I_, J_), x[0])

                elif I_ == J_: # update other blocks on the diag
                    if isinstance(x[0], np.ndarray): 
                        lastDiagBlock, (pMat1, pFlag1), (pMat2, pFlag2) = x
                    elif isinstance(x[1], np.ndarray):
                        (pMat1, pFlag1), lastDiagBlock, (pMat2, pFlag2) = x
                    else:
                        (pMat1, pFlag1), (pMat2, pFlag2), lastDiagBlock = x

                    if pFlag1 == 1: yield((I_, J_), minmpmatmul(pMat2, pMat1, lastDiagBlock))
                    else: yield((I_, J_), minmpmatmul(pMat2, pMat1, lastDiagBlock))
                
                else: # all other blocks
                    if isinstance(x[0], np.ndarray): 
                        lastBlock, (pMat1, pFlag1), (pMat2, pFlag2) = x
                    elif isinstance(x[1], np.ndarray):
                        (pMat1, pFlag1), lastBlock, (pMat2, pFlag2) = x
                    else:
                        (pMat1, pFlag1), (pMat2, pFlag2), lastBlock = x
                    
                    if pFlag1 == -1: yield((I_, J_), minmpmatmul(pMat1, pMat2, lastBlock))
                    else: yield((I_, J_), minmpmatmul(pMat2, pMat1, lastBlock))


        diagBlock = apspGraph.filter(lambda x : x[0][0] == q_ and x[0][1] == q_)\
                             .flatMap(lambda x : share_diag_block(x, q_),\
                              preservesPartitioning=False)\
                             .partitionBy(p, custom_partitioner)
        # filter diagonal block (q,q), do seq fw, and send along row/col

        rowColBlocks = apspGraph.filter(lambda x : x[0][0] == q_ or x[0][1] == q_)\
                                .union(diagBlock)\
                                .combineByKey((lambda x : [x]), \
                                              (lambda x, y : x + [y]), \
                                              (lambda x, y : x + y),\
                                 numPartitions=p,\
                                 partitionFunc=custom_partitioner)\
                                .flatMap(lambda x : updateAndshare(x, q_, q),\
                                 preservesPartitioning=False)\
                                .partitionBy(p, custom_partitioner)
        # UNION (rowColBlocks and diagBlock) AS phase2 and update/compute

        p3Blocks = apspGraph.filter(lambda x : x[0][0] != q_ and x[0][1] != q_)\
                            .union(rowColBlocks)\
                            .combineByKey((lambda x : [x]),\
                                          (lambda x, y : x + [y]),\
                                          (lambda x, y : x + y),\
                             numPartitions=p,\
                             partitionFunc=custom_partitioner)\
                            .mapPartitions(lambda x : UpdateMat(x, q_),\
                             preservesPartitioning=False)\
                            .partitionBy(p, custom_partitioner)
        # UNION (phase2 p3Blocks) AS phase3 and update/compute

        apspGraph = p3Blocks        
        # RESET apspGraph = phase3 (result after update / computation)
        if q_ % 10 == 0: 
            apspGraph.persist(stglev.MEMORY_AND_DISK).checkpoint()
        return apspGraph

    for dq_ in range(0, q, 1):
        apspGraph = run_APSP_diag_iteration(dq_, apspGraph)
        
    
    def doSquareBlock(iter_):
        for ((I_, J_), blockMat) in iter_:
           yield ((I_, J_), np.square(blockMat, out=blockMat))
    
    apspGraph = apspGraph.mapPartitions(lambda x : doSquareBlock(x),\
                          preservesPartitioning=False)
    apspGraph = apspGraph.persist(stglev.MEMORY_AND_DISK)
    apspGraph.count()
    
    print
    print 'All-Pairs Shortest-Paths Stage Completed.'
    print
    print 'APSP Runtime:', time.time() - t_init_apsp, 's.'
    print

    print
    print '- - - - - - - - - - - - - - - - - - - - - - - - - - - -'
    print 
    
    print 'Double Centered Matrix Normalization Step.'
    print
    t_init_norm = time.time()
    
    def compNormSums(((I_, J_), localMat)):
        if I_ == J_:
            yield (J_, np.sum(localMat, axis=0))
        else:
            yield (J_, np.sum(localMat, axis=0))
            yield (I_, np.sum(localMat, axis=1))

    def grandSum(iter_):
        partTotalSum = 0.
        for (J_, subColSum) in iter_:
            partTotalSum += np.sum(subColSum)
        yield partTotalSum
        
    colSums = apspGraph.flatMap(lambda x : compNormSums(x),\
                                preservesPartitioning=False )\
                       .combineByKey((lambda x : x),\
                                     (lambda x, y: x + y),\
                                     (lambda x, y: x + y),\
                                      numPartitions=p)\
                       .persist(stglev.MEMORY_AND_DISK) # used twice below

    grandMean = colSums.mapPartitions(lambda x : grandSum(x)).reduce(_ADD_) / (n*n)

    cRM = colSums\
                 .mapPartitions(lambda x : [(yield ((x_[0], x_[1] / (1. * n) ))) for x_ in x])\
                 .collectAsMap()

    means_bc = sc.broadcast((grandMean, cRM))
    

    def dblCenter(iter_):
        gMean, colRowMeans = means_bc.value        
        for ((I_, J_), locSubMat) in iter_:
            locSubMat -= colRowMeans[J_]
            locSubMat -= colRowMeans[I_].reshape((locSubMat.shape[0], 1))
            locSubMat += gMean
            yield ((I_, J_), -.5*locSubMat) # * -.5


    dblCentMatrix = apspGraph.mapPartitions(lambda x : dblCenter(x))\
                             .persist(stglev.MEMORY_AND_DISK)
    dblCentMatrix.count()

    print
    print '- - - - - - - - - - - - - - - - - - - - - - - - - - - -'
    print  

    print 
    print 'Normalization Runtime:', time.time() - t_init_norm, 's.'
    
    print
    print '- - - - - - - - - - - - - - - - - - - - - - - - - - - -'
    print    
    print 'Power Iteration Eigendecomposition Step.'
    t_init_powit = time.time()
    print


    def blockify(D):
        return [((i, 0), D[a:(a+b), :]) for i, a in enumerate(range(0, n, b))]

    def matrix_multiply(Amat, rightMat):
        piVec = rightMat.value

        def multBlocks(((I_, J_), subMatrix)):
            # map block (i, j) as (i, j) and (j, i) 
            # multiplied to appropriate block
            # of rightMat (bc)
            yield ((I_, 0), np.dot(subMatrix, piVec[(J_, 0)]))
            if I_ != J_: yield ((J_, 0), np.dot(subMatrix.T, piVec[(I_, 0)]) ) # check performance

        # combineByKey to reduce (+)
        # blocks (i, 0) of new V vector
        # return this resulting RDD
        return Amat.flatMap(lambda x : multBlocks(x)).reduceByKey(_ADD_) # RDD of results

    # initial guess 
    Q_, _ = np.linalg.qr(np.eye(n, d), mode='reduced')

    Q_dict = OrderedDict(sorted(blockify(Q_), key=lambda t : t[0]) )
    Q_last = np.vstack(Q_dict.values())

    for i in range(MAX_POWER_ITER):    

        V_bc = sc.broadcast(Q_dict)

        V_ = matrix_multiply(dblCentMatrix, V_bc)
        V_bc.unpersist()

        V_ = OrderedDict(sorted(V_.collectAsMap().items(), key=lambda t : t[0]) )
        V_ = np.vstack((V_.values()))
        Q_, R_ = np.linalg.qr(V_, mode='reduced')

        err_ = np.sum(np.square(Q_ - Q_last))
        if i % 5 == 0:
            print 'Simultaneous Power Iteration error at iteration', i, 'is =', err_
        
        Q_dict = OrderedDict(sorted(blockify(Q_), key=lambda t : t[0]) )
        Q_last = np.vstack(Q_dict.values())

        if err_ < THRESH_POWER_ITER: break # converged

    # after convergence
    # Columns of Q_ are eigenvectors, Diagonal of R_ contains eigenvalues
    # dCoords = Q_ .* ( ones(n, 1) * sqrt(np.diag(R_)) )
    # dCoords = np.multiply( Q_, np.outer(np.ones((n, 1)), np.diag(R_)) )

    dCoords = Q_ * np.sqrt(np.diag(R_))
    np.savetxt(output_dir, dCoords, fmt='%6f', delimiter='\t')

    print 
    print 'Power Iteration Runtime:', time.time() - t_init_powit, 's.'

    print
    print '- - - - - - - - - - - - - - - - - - - - - - - - - - - -'
    print  


    t_end_isomap = time.time()
    print
    print 'Total runtime:', t_end_isomap - t_start_isomap
    
    print
    print 'done.'
